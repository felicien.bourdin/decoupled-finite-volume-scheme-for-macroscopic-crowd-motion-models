import numpy as np
import matplotlib.pyplot as plt
from cvxopt import spmatrix
import cvxopt.solvers
import scipy.sparse as ssp
import scipy.sparse.linalg
from mpl_toolkits.axes_grid1 import make_axes_locatable

cvxopt.solvers.options['show_progress'] = False

#-------------------------------------Spontaneous velocities---------------------------

def U1(x,y):
    return -0.+0*x,-2.+0*x

def U2(x,y):
    return  0+0*x,2.+0*x

#-------------------------------------Resolution of the decoupled scheme---------------------------

# The fields are stored in N(N+1) and (N+1)N arrays.


## Technical transformation function
def scipy_sparse_to_spmatrix(A):
    E=ssp.csr_matrix(A)
    coo = E.tocoo()
    SP = spmatrix(coo.data.tolist(), coo.row.tolist(), coo.col.tolist(), size=A.shape)
    return SP


## Solves the instantaneous decoupled problem
def resolution_decouplage(N,U1,U2,tau,rho1,rho2):
    ## N : meshsize
    ## U1, U2 : spontaneous velocities
    ## tau : timesetp
    ## rho1, rho2 : densities
    ## We builds matrix/vectors P,Q,G,H to write the problem
    ## min 1/2 X^T P X + QX under GX <= H

    ## Format the spontaneous fields in line

    champ1x=U1[0].flatten()
    champ1y=U1[1].flatten()

    champ2x=U2[0].flatten()
    champ2y=U2[1].flatten()

    ## A and b formulations stem from the term \rho |u-U|^2 (linear and quadratic parts)
    A = np.zeros(8 * N*N) ## further P what encodes the 2-norm weighted by the densities
    b = np.zeros(8 * N*N) ## further Q that supports the densities


    for i in range(N):
        for j in range(N):
            #u1x+
            A[i*N+j ] = rho1[i , j-1]
            #u1x-
            A[i*N+j  + N * N] = rho1[i , j]
            #u1x+
            b[i*N+j ] = -rho1[i , j-1] * champ1x[i*N + j]
            #u1x-
            b[i*N+j + N * N] = rho1[i , j] * champ1x[i*N + j]

    for i in range(N):
        for j in range(N):
            #u1y+
            A[i*N+j+ 2 * N * N] = rho1[i-1, j ]
            #u1y-
            A[i*N+j + 3 * N * N] = rho1[i, j]
            #u2y+
            b[i*N+j + 2 * N * N] = -rho1[i-1, j] * champ1y[i*N + j ]
            #u2y-
            b[i*N+j + 3 * N * N] = rho1[i, j] * champ1y[i*N + j ]


    for i in range(N):
        for j in range(N):
            A[i*N + j + 4 * N * N] = rho2[i , j-1]
            A[i*N + j  + N * N + 4 * N * N] = rho2[i , j]
            b[i*N + j + 4 * N * N] = -rho2[i , j-1] * champ2x[i*N + j]
            b[i*N + j  + N * N+ 4 * N * N] = rho2[i , j] * champ2x[i*N + j]

    for i in range(N):
        for j in range(N):
            A[i*N + j  + 2 * N * N + 4 * N * N] = rho2[i-1, j ]
            A[i*N + j  + 3 * N * N + 4 * N * N] = rho2[i, j ]
            b[i*N + j  + 2 * N * N + 4 * N * N] = -rho2[i-1, j ] * champ2y[i*N + j  ]
            b[i*N + j  + 3 * N * N + 4 * N * N] = rho2[i, j ] * champ2y[i*N + j ]

    ## One shall keep only positive entries to have a well-posed problem
    A[A<0]=0
    A=np.round(A,2)
    indices = np.where(A!=0)[0]
    A = A[indices]
    print("minimax:",np.max(A),np.min(A),rho1.max(),rho2.max())

    b = b[indices]
    Ptest=np.diag(A)
    ## Display rank and conditioning
    print("rang : ",np.linalg.matrix_rank(Ptest),"sur", Ptest.shape[1],"cond P", np.linalg.cond(Ptest))


    ## Format to have compatible data with cvxopt
    A = ssp.diags(A)
    P = scipy_sparse_to_spmatrix(A)
    Q = cvxopt.matrix(b)

    ## There are two types of contraints : the sum has to remain <1 - the densities has to be positibe

    B = ssp.lil_matrix((N**2,8*N*N)) ## 1st contraint
    D = ssp.lil_matrix((2*N**2,8*N*N)) ## 2nd constraint
    for k in range(N):
        for l in range(N):
            #u1x+
            B[k*N+l,k*N+l] = rho1[k,l-1]
            B[k*N+l,k*N+((l+1)%N)] = -rho1[k,l]
            #u1x-
            B[k*N+l,N*N+k*N+l] = -rho1[k,l]
            B[k*N+l,N*N+k*N+((l+1)%N)] = rho1[k,(1+l)%N]
            #u1y+
            B[k*N+l,2*N*N+k*N+l] = rho1[k-1,l]
            B[k*N+l,2*N*N+(k+1)%N*N+l] = -rho1[k,l]
            #u1y-
            B[k*N+l,3*N*N+k*N+l] = -rho1[k,l]
            B[k*N+l,3*N*N+(k+1)%N*N+l] = rho1[(k+1)%N,l]
            #
            #u2x+
            B[k*N+l,4*N*N+k*N+l] = rho2[k,l-1]
            B[k*N+l,4*N*N+k*N+((l+1)%N)] = -rho2[k,l]
            #u2x-
            B[k*N+l,4*N*N+N*N+k*N+l] = -rho2[k,l]
            B[k*N+l,4*N*N+N*N+k*N+((l+1)%N)] = rho2[k,(1+l)%N]
            #u2y+
            B[k*N+l,4*N*N+2*N*N+k*N+l] = rho2[k-1,l]
            B[k*N+l,4*N*N+2*N*N+(k+1)%N*N+l] = -rho2[k,l]
            #u2y-
            B[k*N+l,4*N*N+3*N*N+k*N+l] = -rho2[k,l]
            B[k*N+l,4*N*N+3*N*N+(k+1)%N*N+l] = rho2[(k+1)%N,l]
    for k in range(N):
        for l in range(N):
            D[k * N + l, k + l * N] = rho1[k, l - 1]
            D[k * N + l, k + ((l + 1) % N) * N] = -rho1[k, l]
            # u1x-
            D[k * N + l, N * N + k + N * l] = -rho1[k, l]
            D[k * N + l, N * N + k + ((l + 1) % N) * N] = rho1[k, (1 + l) % N]
            # u1y+
            D[k * N + l, 2 * N * N + k + N * l] = rho1[k - 1, l]
            D[k * N + l, 2 * N * N + (k + 1) % N + N * l] = -rho1[k, l]
            # u1y-
            D[k * N + l, 3 * N * N + k + N * l] = -rho1[k, l]
            D[k * N + l, 3 * N * N + (k + 1) % N + N * l] = rho1[(k + 1) % N, l]

    for k in range(N):
        for l in range(N):
            # u2x+
            D[k * N + l, 4 * N * N + k + l * N] = rho2[k, l - 1]
            D[k * N + l, 4 * N * N + k + ((l + 1) % N) * N] = -rho2[k, l]
            # u2x-
            D[k * N + l, 4 * N * N + N * N + k + N * l] = -rho2[k, l]
            D[k * N + l, 4 * N * N + N * N + k + ((l + 1) % N) * N] = rho2[k, (1 + l) % N]
            # u2y+
            D[k * N + l, 4 * N * N + 2 * N * N + k + N * l] = rho2[k - 1, l]
            D[k * N + l, 4 * N * N + 2 * N * N + (k + 1) % N + N * l] = -rho2[k, l]
            # u2y-
            D[k * N + l, 4 * N * N + 3 * N * N + k + N * l] = -rho2[k, l]
            D[k * N + l, 4 * N * N + 3 * N * N + (k + 1) % N + N * l] = rho2[(k + 1) % N, l]

    ## Minimisation only on index where the density does not vanish

    Bindices = B[:,indices]
    Dindices = D[:,indices]
    D=ssp.csr_matrix(tau*Dindices)
    Bindices=ssp.csr_matrix(tau*Bindices)
    E = -ssp.eye(8*N*N,8*N*N)
    E = ssp.csr_matrix(E)
    Eindices = E[:,indices]

    G=ssp.vstack((Bindices,-D,Eindices))
    G=scipy_sparse_to_spmatrix(G)

    H=cvxopt.matrix(0.0,(N**2*11,1))
    for k in range(N):
        for l in range(N):
            H[k*N+l,0]=1-rho1[k,l]-rho2[k,l]
            H[N**2+k*N+l,0]=rho1[k,l]
            H[2*N**2+k*N+l,0]=rho2[k,l]


    ## Optimize with cvxopt

    solution=np.array(cvxopt.solvers.qp(P=P,q=Q,G=G,h=H,solver='MOSEK')['x'])

    ## Build the new solution
    solution_remplie = np.zeros(8*N*N)
    for id,ip in enumerate(indices):
        solution_remplie[ip]=solution[id]

    ## Build corresponding vectors

    u1Posx=solution_remplie[0:N*N]
    u1Negx=solution_remplie[N*N:2*N*N]
    u1Posy=solution_remplie[2*N*N:3*N*N]
    u1Negy=solution_remplie[3*N*N:4*N*N]

    u2Posx=solution_remplie[4*N*N+0:4*N*N+N*N]
    u2Negx=solution_remplie[4*N*N+N*N:4*N*N+2*N*N]
    u2Posy=solution_remplie[4*N*N+2*N*N:4*N*N+3*N*N]
    u2Negy=solution_remplie[4*N*N+3*N*N:4*N*N+4*N*N]

    u1 = np.hstack([u1Posx,u1Negx,u1Posy,u1Negy])
    u2 = np.hstack([u2Posx,u2Negx,u2Posy,u2Negy])
    print("max vitesses : ",u1.max(),u2.max())

    ## Compute the transported densities

    densite1=np.zeros((N,N))
    densite2=np.zeros((N,N))

    for k in range(N):
        for l in range(N):
            densite1[k,l] = rho1[k,l] +  tau*B[k*N+l,:4*N*N].dot(u1)
            densite2[k,l] = rho2[k,l] +  tau*B[k*N+l,4*N*N:].dot(u2)

    u1x=u1Posx-u1Negx
    u1x=u1x.reshape((N,N))
    u1y=u1Posy-u1Negy
    u1y=u1y.reshape((N,N))

    u2x=u2Posx-u2Negx
    u2x=u2x.reshape((N,N))
    u2y=u2Posy-u2Negy
    u2y=u2y.reshape((N,N))

    return u1x,u1y,densite1, u2x, u2y, densite2


## Operator that refines the mesh (for display)
def glob(ux,uy):
    N=ux.shape[0]
    uxglob=np.zeros((2*N,2*N))
    uyglob=np.zeros((2*N,2*N))
    for k in range(N):
        for l in range(N):
                uxglob[2*k,2*l]=ux[k,l]
                uxglob[2*k+1,2*l]=ux[k,l]
                uxglob[2*k,2*l+1]=ux[k,l+1]
                uxglob[2*k+1,2*l+1]=ux[k,l+1]

                uyglob[2*k,2*l]=uy[k,l]
                uyglob[2*k+1,2*l]=uy[k,l]
                uyglob[2*k,2*l+1]=uy[k+1,l]
                uyglob[2*k+1,2*l+1]=uy[k+1,l]

    return uxglob, uyglob


## Display results

def graphique(N,champ1,champ2,uxglob1,uyglob1,uxglob2,uyglob2,densite1ini,densite2ini, densite1f, densite2f):


    ## quiver field 1
    ax = plt.subplot(241)
    x = np.linspace(0,1,N)
    y = np.linspace(0,1,N)
    xx, yy = np.meshgrid(x, y)
    xxU = champ1(xx,yy)[0]
    yyU = champ1(xx,yy)[1]
    ax.quiver(xx,yy,xxU,yyU)
    ax.set_xticks(x)
    ax.set_yticks(x)
    plt.gca().set_aspect('equal', adjustable='box')

    ## quiver field 2
    ax = plt.subplot(245)
    x = np.linspace(0,1,N)
    y = np.linspace(0,1,N)
    xx, yy = np.meshgrid(x, y)
    xxU = champ2(xx,yy)[0]
    yyU = champ2(xx,yy)[1]
    ax.quiver(xx,yy,xxU,yyU)
    ax.set_xticks(x)
    ax.set_yticks(x)
    plt.gca().set_aspect('equal', adjustable='box')

    ## quiver resulting field1
    ax = plt.subplot(242)
    x = np.linspace(1 / 4 / N, 1 - 1 / 4 / N, 2 * N)
    y = np.linspace(1 / 4 / N, 1 - 1 / 4 / N, 2 * N)
    xx, yy = np.meshgrid(x, y)
    ax.quiver(xx, yy, uxglob1, uyglob1)
    x = np.linspace(0, 1, N + 1)
    ax.set_xticks(x)
    ax.set_yticks(x)
    plt.gca().set_aspect('equal', adjustable='box')

    ## quiver resulting field2
    ax = plt.subplot(246)
    x = np.linspace(1 / 4 / N, 1 - 1 / 4 / N, 2 * N)
    y = np.linspace(1 / 4 / N, 1 - 1 / 4 / N, 2 * N)
    xx, yy = np.meshgrid(x, y)
    ax.quiver(xx, yy, uxglob2, uyglob2)
    x = np.linspace(0, 1, N + 1)
    ax.set_xticks(x)
    ax.set_yticks(x)
    plt.gca().set_aspect('equal', adjustable='box')

    ## imshow densite1 ini
    ax = plt.subplot(243)
    ax.imshow(densite1ini)

    ## imshow densite2 ini
    ax = plt.subplot(247)
    ax.imshow(densite2ini)

    ## imshow densite1 f
    ax = plt.subplot(244)
    ax.imshow(densite1f)

    ## imshow densite2 f
    ax = plt.subplot(248)
    ax.imshow(densite2f)

    plt.show()

def mat_Laplacien_2D(N): ## builds +Laplacian matrix in two 2
    dx=1/N
    Id1d = ssp.identity(N)
    ones = np.ones(N)
    diag = 2*ones
    aux = [diag, -ones[:-1], -ones[:-1],[-1],[-1]]
    Lap1d = ssp.diags(aux, [0, -1, 1,-(N-1),N-1])
    Lap2d = ssp.kron(Id1d, Lap1d) + ssp.kron(Lap1d, Id1d)
    return -Lap2d/dx/dx

def champ_chemo(N,rho_1,rho_2,alpha_1=1,alpha_2=1):
    ## Solves -Lap (rho_1+rho_2) = C, i.e. the chemiodiffusion
    Lap2d = mat_Laplacien_2D(N)
    Id = ssp.identity(N * N)
    A1 = alpha_1 * Id - Lap2d
    B1 = rho_1.reshape((N * N))
    A2 = alpha_2 * Id - Lap2d
    B2 = rho_2.reshape((N * N))
    potentiel_1 = ssp.linalg.spsolve(A1, B1 + B2)  # -Lapl=rho
    potentiel_2 = ssp.linalg.spsolve(A2, B1 + B2)
    potentiel_1 = potentiel_1.reshape((N, N))
    potentiel_2 = potentiel_2.reshape((N, N))
    return potentiel_1, potentiel_2

def grad_pot(potentiel_1,potentiel_2):
    ## Computes the gradient of a potential function
    N=potentiel_1.shape[0]
    dx=1/N
    x_vel_1 = np.zeros((N + 1, N))
    y_vel_1 = np.zeros((N, N + 1))
    x_vel_2 = np.zeros((N + 1, N))
    y_vel_2 = np.zeros((N, N + 1))

    for i in range(N + 1):
        for j in range(N):
            x_vel_1[i, j] = -(potentiel_1[i%N ,j]-potentiel_1[i-1,j])/dx
            y_vel_1[j, i] = -(potentiel_1[j,i%N]-potentiel_1[j,i-1])/dx
            x_vel_2[i, j] = -(potentiel_2[i%N,j]-potentiel_2[i-1,j])/dx
            y_vel_2[j, i] = -(potentiel_2[j,i%N]-potentiel_2[j,i-1])/dx
    return x_vel_1,x_vel_2,y_vel_1,y_vel_2

def flux(u_1,u_2,potentiel_1,potentiel_2, N):
    ## Compute the flux associated to velocities + grad(potientials)

    x_vel_1,x_vel_2,y_vel_1,y_vel_2 = grad_pot(potentiel_1,potentiel_2)
    for i in range(N + 1):
        for j in range(N):
            x_vel_1[i, j] += u_1(i / N, j / N)[0]
            y_vel_1[j, i] += u_1(j / N, i / N)[1]
            x_vel_2[i, j] += u_2(i / N, j / N)[0]
            y_vel_2[j, i] += u_2(j / N, i / N)[1]

    return [x_vel_1, y_vel_1],[x_vel_2, y_vel_2]

def schema(tau, tmax, rho1, rho2, U1, U2, N):
    ## tau : timestep
    ## tmax : totaltime
    ## rho_1/2 : initial densities
    ## U1/2 : spontaneous velocities
    ## N : meshsite
    Nit = int(tmax/tau)
    densites1 = np.zeros((rho1.shape[0],rho1.shape[1],Nit+1))
    densites2 = np.zeros((rho2.shape[0],rho2.shape[1],Nit+1))
    densite1_pas = rho1
    densite2_pas = rho2
    ## To further include chemoattraction - optional
    pot1,pot2 = champ_chemo(N,rho1,rho2)
    U_1,U_2 = flux(U1,U2,0*pot1,0*pot2,N)
    for k in range(Nit):
        densites1[:,:,k] = densite1_pas
        densites2[:,:,k] = densite2_pas
        a,b,densite1_pas,d,e,densite2_pas = resolution_decouplage(N,U_1,U_2,tau,densite1_pas,densite2_pas)
        print("pas ", k , " sur ",Nit)
        print("sommes : ", np.sum(densite1_pas),np.sum(densite2_pas))
        print(densite1_pas.max(),densite2_pas.max())


        ## Display and save densities
        o=k

        fig = plt.figure(figsize=(32, 8))
        ax1 = fig.add_subplot(1, 3, 2)
        im1 = ax1.imshow(densites1[:, :, o], interpolation='None', origin='lower', vmin=0, vmax=1,cmap = "Greys")
        divider = make_axes_locatable(ax1)
        cax = divider.append_axes('right', size='5%', pad=0.05)
        fig.colorbar(im1, cax=cax, orientation='vertical')

        ax2 = fig.add_subplot(1, 3, 3)
        im2 = ax2.imshow(densites2[:, :, o], interpolation='None', origin='lower',vmin=0, vmax=1)
        divider = make_axes_locatable(ax2)
        cax = divider.append_axes('right', size='5%', pad=0.05)
        fig.colorbar(im2, cax=cax, orientation='vertical')

        ax3 = fig.add_subplot(1, 3, 1)
        im3 = ax3.imshow(densites1[:, :, o] + densites2[:, :, o], interpolation='None', origin='lower',vmin=0, vmax=1, cmap =  "Greys")
        divider = make_axes_locatable(ax3)
        cax = divider.append_axes('right', size='5%', pad=0.05)
        fig.colorbar(im3, cax=cax, orientation='vertical')

        fig.savefig('images/image%o' % o)
        plt.close()


    densites1[:,:,-1] = densite1_pas
    densites2[:,:,-1] = densite2_pas
    return densites1,densites2

if __name__ == '__main__':
    tau=0.05 ## Timestep
    N=80 ## Meshsite
    tmax = 10 ##total time
    rho_1 = np.zeros((N, N))
    rho_2 = np.zeros((N, N))

    ## Initial densities : two discs

    r = N / 12  # radius


    for i in range(N):
        for j in range(N):
            if (j - N / 2) ** 2 + (i - 2 * N / 5) ** 2 < r ** 2:
                rho_2[i, j] += 1

    for i in range(N):
        for j in range(N):
            if (j - N / 2) ** 2 + (i - 3 * N / 5) ** 2 < r ** 2:
                rho_1[i, j] += 1


    densites1,densites2 = schema(tau, tmax, rho_1, rho_2, U1, U2, N)