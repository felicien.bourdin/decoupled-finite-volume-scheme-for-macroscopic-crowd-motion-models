# Decoupled finite volume scheme for macroscopic crowd motion models

Project realized by Félicien BOURDIN during his PhD Thesis.
The work of Félicien BOURDIN is supported by the ERC Grant NORIA.

Motion of a crowd divided in two types rho_1$ and rho_2 subjected to two velocity fields U_1 and U_2 under the congestion constraint rho_1+rho_2.

A finite volume scheme is computed here. We replaced the velocities defined at the edges by pairs of velocities (U_-,U_+) - the negative and the positive part, in order to have a linear dependancy on the fields. 

At each step of the scheme, the desired velocities are projected onto the set of velocities whose application by the finite volume scheme preserves the congestion constraint rho_1+rho_2.
